import pandas as pd
from IPython.display import display
import irpf

# Place the absolute file paths in here
filepath_list = [
        "ABSOLUTE/PATH/TO/YOUR/B3/FILES",
]

# Place the name of your broker
minha_corretora = "EXEMPLO"

def main():

    acoes_df = []
    for filepath in filepath_list:
        acoes_df_filepath = pd.read_excel(filepath, sheet_name="Negociação")
        acoes_df.append(acoes_df_filepath)

    acoes_df = pd.concat([*acoes_df])

    acoes_df_group_ticker = acoes_df.groupby('Código de Negociação')

    acoes_list = []

    # ticker = 'ENBR3F'
    # df = acoes_df_group_ticker.get_group(ticker)

    for ticker, df in acoes_df_group_ticker :
        print(ticker)

        df.loc[:, 'Ano'] = df['Data do Negócio'].apply(lambda x : x.split('/')[-1])

        df_compra = df[df['Tipo de Movimentação'] == 'Compra']
        df_venda  = df[df['Tipo de Movimentação'] == 'Venda']

        # Preco Medio
        sum_total = df_compra['Valor'].sum()

        qnt_total_compra = df_compra['Quantidade'].sum()
        qnt_total_venda  = df_venda['Quantidade'].sum()
        qnt_total        = qnt_total_compra - qnt_total_venda

        preco_medio = round(sum_total / qnt_total_compra, 2)

        # Quantidade ano
        df_compra_ano = df_compra.groupby('Ano').sum()
        df_venda_ano  = df_venda.groupby('Ano').sum()

        d = irpf.get_ticker_info(ticker)

        try:
            corretora = df_compra["Instituição"].iloc[0]
        except:
            corretora = minha_corretora

        # x = f"{qnt_total} ACOES DA {d["Nome"].upper()}"
        discriminacao_text = irpf.get_discriminacao_text(d["Nome"], qnt_total, d["Tipo"])
        discriminacao      = f"{discriminacao_text}. CODIGO DE NEGOCIACAO B3: {ticker}. PREÇO MÉDIO: R${preco_medio}. CNPJ: {d['CNPJ'].upper()}. CORRETORA: {corretora}"

        for idx, r in df_compra_ano.cumsum().iterrows() :
            # d[f"Quantidade_{idx}"] = r["Quantidade"]
            d[f"Valor_Compra_{idx}"] = round(r["Valor"], 2)

        output = {
            "Ticker" : ticker,
            "Quantidade total" : qnt_total,
            "Preço médio" : preco_medio,
            "Discriminação" : discriminacao,
            **d
        }

        acoes_list.append(output)
    df_output = pd.DataFrame(acoes_list)
    df_output.to_excel("../data/Output_IR_2021.xlsx", index=False)


main()