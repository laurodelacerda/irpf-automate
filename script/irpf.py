import requests
from bs4 import BeautifulSoup


def get_bdr_info(ticker):
    """ Get info about one BDR from statusinvest.

    :param ticker: The ticker from B3
    :return: The mapping about one BDR
    """
    
    URL_BASE_BDRS = "https://statusinvest.com.br/bdrs"
    URL = f"{URL_BASE_BDRS}/{ticker}"

    d = {}

    try :
        req = requests.get(URL, {})
        soup = BeautifulSoup(req.content, "html.parser")

        div_company_description = soup.find("div", {"class" : "company-description"})
        data = [i.get_text() for i in div_company_description.select(".d-block")]

        d["Nome"] = data[0]
        d["Tipo"] = "BDR"

    except:
        pass

    return d


def get_stock_info(ticker):
    """ Get info about one ticker from statusinvest.

    :param ticker: The ticker from B3 
    :return: The mapping about one ticker 
    """
    
    URL_BASE_ACOES = "https://statusinvest.com.br/acoes"

    if len(ticker) < 6:
        URL = f"{URL_BASE_ACOES}/{ticker}"
    else:
        URL = f"{URL_BASE_ACOES}/{ticker[:-1]}"

    d = {}

    try :
        req = requests.get(URL, {})
        soup = BeautifulSoup(req.content, "html.parser")

        div_company_description = soup.find("div", {"class" : "company-description"})

        data = [i.get_text() for i in div_company_description.select(".d-block")]

        d["Nome"] = data[0]
        d["CNPJ"] = data[1]
        d["Tipo"] = "Ação"

    except Exception as e :
        pass

    return d


def get_fii_info(ticker):
    """ Get info about one FII from statusinvest.

    :param ticker: The ticker from B3
    :return: The mapping about one FII 
    """
    
    URL_BASE_FII = "https://statusinvest.com.br/fundos-imobiliarios"
    URL = f"{URL_BASE_FII}/{ticker}"

    d = {}

    try :
        req = requests.get(URL, {})
        soup = BeautifulSoup(req.content, "html.parser")

        div_top_info = soup.select_one("div.top-info.top-info-1.top-info-md-2")
        div_info = div_top_info.select(".info")

        for div in div_info :
            key = div.select_one("h3.title").text
            value = div.select_one("strong.value").text
            d[key] = value

        d["Nome"] = d.pop("Nome Pregão")
        d["Tipo"] = "FII"

    except Exception as e :
        pass

    return d


def get_etf_info(ticker):
    """ Get info about one ETF from statusinvest.

    :param ticker: The ticker from B3
    :return: The mapping about one ETF
    """

    URL_BASE_ETF = "https://statusinvest.com.br/etfs"
    URL = f"{URL_BASE_ETF}/{ticker}"

    d = {}

    try :
        req = requests.get(URL, {})
        soup = BeautifulSoup(req.content, "html.parser")

        etf_name = soup.select_one("#company-section div h4 span").text

        div_top_info = soup.select_one("#company-section  div  div.top-info.info-3.sm.d-flex.justify-between")
        div_info = div_top_info.select(".info")

        for div in div_info :
            key = div.select_one("h3.title").text
            value = div.select_one("strong.value").text
            d[key] = value

        d["Nome"] = etf_name
        d["Tipo"] = "ETF"

    except :
        pass

    return d


def get_ticker_info(ticker):
    """ Collect info about one ticker from the methods above.

    :param ticker: The ticker from B3
    :return: The mapping about one ticker
    """

    d_stock = get_stock_info(ticker)
    d_fii   = get_fii_info(ticker)
    d_etf   = get_etf_info(ticker)
    d_bdr   = get_bdr_info(ticker)

    name       = d_stock.get("Nome", d_fii.get("Nome", d_etf.get("Nome", d_bdr.get("Nome", ""))))
    cnpj       = d_stock.get("CNPJ", d_fii.get("CNPJ", d_etf.get("CNPJ", d_bdr.get("CNPJ", ""))))
    asset_type = d_stock.get("Tipo", d_fii.get("Tipo", d_etf.get("Tipo", d_bdr.get("Tipo", ""))))

    d = {
        "Nome" : name,
        "CNPJ" : cnpj,
        "Tipo" : asset_type
    }

    return d


def get_discriminacao_text(name, qnt_total, asset_type):
    """ Get info about field discriminação to place on IRPF.

    :param name:        The name of the company that offers the stock
    :param qnt_total:   The number of stocks
    :param asset_type:  The type of stock investment

    :return: The text to place on field discriminação on IRPF.
    """

    dict_discriminacao_text = {
        "Ação" : f"{qnt_total} AÇÕES DA {name.upper()}",
        "FII" :  f"{qnt_total} COTAS DO FUNDO IMOBILIÁRIO {name.upper()}",
        "ETF" :  f"{qnt_total} COTAS DO ETF {name.upper()}",
        "BDR" :  f"{qnt_total} COTAS DO BDR {name.upper()}"
    }

    return dict_discriminacao_text[asset_type] if asset_type else {}
